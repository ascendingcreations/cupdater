/* 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Author(s):
 *  - S.J.R. van Schaik <stephan@synkhronix.com>
 */
#pragma once

#include <stdio.h>
#include <stdlib.h>

enum shine_token_kind {
	TOKEN_END, TOKEN_COMMA, TOKEN_SEMICOLON, TOKEN_LPAREN, TOKEN_RPAREN,
	TOKEN_LSQUARE, TOKEN_RSQUARE, TOKEN_LBRACE, TOKEN_RBRACE, TOKEN_QUOTATION,
	TOKEN_STR, TOKEN_CONST,
};

struct shine_cursor {
	unsigned int line, col;
};

#define SHINE_CURSOR(...) ((struct shine_cursor){ __VA_ARGS__ })

struct shine_scanner {
	struct shine_cursor cursor, next_cursor;
	FILE *file;
	unsigned int tab_width;
	char byte;
};

int shine_init_scanner(struct shine_scanner *scanner, FILE *file);
char shine_next_scanner_byte(struct shine_scanner *scanner);
