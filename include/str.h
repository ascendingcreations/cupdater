/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Author(s):
 *  - Andrew Wheeler <lordsatin@hotmail.com>
 * 	- S.J.R. van Schaik <stephan@synkhronix.com>
 */

#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>

struct swift_str {
	size_t len, size;
	char *data;
};

#define swift_str_len(s) ((s)->len)

char *swift_strdup(const char *s);
char *swift_strndup(const char *s, size_t n);
struct swift_str *swift_alloc_str(size_t size);
struct swift_str *swift_resize_str(struct swift_str *str, size_t size);
void swift_free_str(struct swift_str *str);
bool swift_cmp_str(struct swift_str *lhs, struct swift_str *rhs);
struct swift_str *swift_clone_str(struct swift_str *str);
int swift_push_char(struct swift_str **lhs, const char rhs);
int swift_push_cstr(struct swift_str **lhs, const char *rhs);
int swift_push_str(struct swift_str **lhs, struct swift_str *rhs);
int swift_push_strf(struct swift_str **str, const char *fmt, ...);
int swift_join_cstrs(struct swift_str **str, size_t count, char **strs,
	char *sep);
int swift_substr(struct swift_str *str, size_t offset, size_t len);
int swift_str_subst(struct swift_str **tgt, const char *pat, const char *repl);
int swift_truncate_str(struct swift_str *str, size_t len);
