#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <scanner.h>

enum shine_type {
	TYPE_INT, TYPE_STR,
};

enum shine_id_type {
	TYPE_NONE, TYPE_PATH, TYPE_FILE,
};

struct script_obj {
	char *url, *path, *name, *next_id;
	struct shine_scanner scanner;
	int type, ver;
};

struct shine_property {
	const char *name;
	enum shine_type type;
	size_t offset;
};

struct shine_id_types {
	const char *name;
	enum shine_id_type type;
};

int shine_find_id(const char *name);
struct shine_property *shine_find_prop(const char *name);
int shine_set_obj_prop(struct script_obj *obj, const char *name, ...);
