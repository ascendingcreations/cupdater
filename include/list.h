/* 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Author(s):
 *  - S.J.R. van Schaik <stephan@synkhronix.com>
 */

#pragma once

#include <stdbool.h>

struct swift_list;

struct swift_list {
	struct swift_list *prev, *next;
};

#define SWIFT_LIST_INIT(name) { &(name), &(name) }
#define SWIFT_LIST(name) \
	struct swift_list name = SWIFT_LIST_INIT(name)

#define swift_foreach_list_item(list, item) \
	for (item = (list)->next; item != (list); item = item->next)
#define swift_foreach_list_item_safe(list, item, next_item) \
	for (item = (list)->next, next_item = item->next; item != (list); \
		item = next_item, next_item = next_item->next)

#define swift_foreach_list_item_rev(list, item) \
	for (item = (list)->prev; item != (list); item = item->prev)
#define swift_foreach_list_item_rev_safe(list, item, prev_item) \
	for (item = (list)->prev, prev_item = item->prev; item != (list); \
		item = prev_item, prev_item = prev_item->prev)

void swift_init_list(struct swift_list *list);
bool swift_is_list_empty(struct swift_list *list);
int swift_insert_list_item_before(struct swift_list *node,
	struct swift_list *item);
int swift_insert_list_item_after(struct swift_list *node,
	struct swift_list *item);
int swift_push_list_item(struct swift_list *list, struct swift_list *item);
int swift_unshift_list_item(struct swift_list *list, struct swift_list *item);
int swift_remove_list_item(struct swift_list *item);
struct swift_list *swift_pop_list_item(struct swift_list *list);
struct swift_list *swift_shift_list_item(struct swift_list *list);

