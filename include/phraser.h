#pragma once

#include <list.h>
#include <macros.h>
#include <scanner.h>

struct item_data {
	char *url, *path, *name;
	struct swift_list node;
};

enum shine_gui_errors {
	ERROR_INVALID_ID = -1,
	ERROR_INVALID_ID_ENDING = -2,
	ERROR_COMMENT_BROKEN = -3,
	ERROR_MISSING_QUOTE = -4,
	ERROR_NOT_CONST = -5,
	ERROR_NOT_FLOATING_POINT = -6,
	ERROR_NOT_FUNCTION = -7,
	ERROR_INIT_SCANNER = -8,
	ERROR_NO_MEMORY = -9,
	ERROR_NULL_STRING = -10,
	ERROR_UNKNOWN = -11,
	ERROR_NONE = 0,
};

void unload_item(struct item_data *data);
int parse_updates(struct swift_list *list);