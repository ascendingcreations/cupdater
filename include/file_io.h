/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Author(s):
 *  - S.J.R. van Schaik <stephan@synkhronix.com>
 *  - Andrew Wheeler <lordsatin@hotmail.com>
 */
#pragma once

#include <stdbool.h>

void create_folder(const char *path);
int download_file(const char *url, const char *output);
int run_process(const char *path);
char *load_file(const char *path);
void rename_file(const char *path, const char *filename, const char *newname);
bool file_exists(const char *path);
void delete_file(const char *path);