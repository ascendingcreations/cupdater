/* 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Author(s):
 *  - S.J.R. van Schaik <stephan@synkhronix.com>
 */

#pragma once
#include <limits.h>
#include <stddef.h>

#define URL "https://ascendingcreations.com/downloads/updates.up"
#define OLD_SCRIPT "updates.up"
#define NEW_SCRIPT "new_updates.up"

#define swift_bit_size_of(x) (sizeof(x) * CHAR_BIT)

#define swift_container_of(ptr, type, member) \
	((type *)((char *)(ptr) - offsetof(type, member)))

#define swift_count_of(array) \
	(sizeof(array) / sizeof(*(array)))

