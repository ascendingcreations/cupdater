#include <stdio.h>
#include <str.h>
#include <file_io.h>
#include <string.h>
#include <phraser.h>
#include <ctype.h>
#include <obj_props.h>
#include <macros.h>

#define shine_free(ptr) \
		if (ptr) \
			free(ptr); \
		ptr = NULL

#define shine_error_free(ptr, err) \
				shine_free(ptr); \
				return err

#define shine_error_msg(cursor, id) \
	printf("Error: %s, line: %i, Column: %i \n", \
		shine_err_to_str[abs(id)], cursor.line, cursor.col); \
		return id

const char *shine_err_to_str[] = {
	[0] = "No Errors",
	[1] = "The ID does not exist or is spelt wrong",
	[2] = "The ID must end with an :. No spaces can be in " \
		"the ID or before the :",
	[3] = "Comments can either start with an // for a " \
		"single lined comment or start with an /* and end with an */ " \
		"for a multi lined comment",
	[4] = "You must have a \" before and after a string",
	[5] = "A constant can only contain numbers 0 thru 9",
	[6] = "A Floating point can only contain 0 thru 9 "\
		"and a period",
	[7] = "The function does not exist or is spelt wrong",
	[8] = "The scanner failed to intialize",
	[9] = "There is not enough room in Ram",
	[10] = "The string failed to create",
	[11] = "Opps this was a unpredictable error",
};

static int shine_parse_token(struct shine_scanner *scanner);

static inline int shine_consume_byte(struct shine_scanner *scanner,
	enum shine_token_kind token_kind)
{
	shine_next_scanner_byte(scanner);
	return token_kind;
}

static inline int shine_parse_whitespace(struct shine_scanner *scanner)
{
	while (isspace(scanner->byte))
		shine_next_scanner_byte(scanner);

	return shine_parse_token(scanner);
}

static int shine_parse_comment(struct shine_scanner *scanner)
{
	switch (shine_next_scanner_byte(scanner)) {
		case '/': {
			while (shine_next_scanner_byte(scanner) != '\n');

		} return shine_parse_token(scanner);

		case '*': {
			struct shine_cursor cur = scanner->cursor;

			shine_next_scanner_byte(scanner);

			do {
				while (scanner->byte != '*') {
					if (scanner->byte == '\0') {
						scanner->cursor = cur;
						return ERROR_COMMENT_BROKEN;
					}

					shine_next_scanner_byte(scanner);
				}
			} while (shine_next_scanner_byte(scanner) != '/');

			shine_next_scanner_byte(scanner);

			return shine_parse_token(scanner);
		}
	}

	return ERROR_COMMENT_BROKEN;
}

static int shine_parse_const(struct shine_scanner *scanner, int *i)
{
	struct swift_str *string = NULL;

	if (shine_parse_token(scanner) != TOKEN_CONST)
		return ERROR_NOT_CONST;

	while (isdigit(scanner->byte)) {
		if (swift_push_char(&string, scanner->byte) < 0 ) {
			shine_error_free(string, ERROR_NO_MEMORY);
		}

		shine_next_scanner_byte(scanner);
	}

	if (string == NULL)
		return ERROR_NULL_STRING;

	if (scanner->byte == '.') {
		shine_error_free(string, ERROR_NOT_CONST);
	}

	*i = strtol(string->data, NULL, 10);

	shine_error_free(string, ERROR_NONE);
}

static int shine_parse_string(struct shine_scanner *scanner, char **str)
{
	struct swift_str *string = NULL;

	if (shine_parse_token(scanner) != TOKEN_QUOTATION) 
		return ERROR_MISSING_QUOTE;

	while (isalpha(scanner->byte) || isdigit(scanner->byte) ||
		scanner->byte == '_' || scanner->byte == ' ' || scanner->byte == '/' 
			|| scanner->byte == '.' || scanner->byte == ':') {
		if (swift_push_char(&string, scanner->byte) < 0 ) {
			shine_error_free(string, ERROR_NO_MEMORY);
		}

		shine_next_scanner_byte(scanner);
	}

	if (string == NULL)
		return ERROR_NULL_STRING;

	if (scanner->byte != '"') {
		shine_error_free(string, ERROR_MISSING_QUOTE);
	} else
		shine_next_scanner_byte(scanner);

	if (!(*str = swift_strndup(string->data, string->len))) {
		shine_error_free(string, ERROR_NO_MEMORY);
	}

	shine_error_free(string, ERROR_NONE);
}

static int shine_parse_identifier(struct shine_scanner *scanner, char **str)
{
	struct swift_str *string = NULL;

	if (*str)
		return ERROR_NONE;

	while (isalpha(scanner->byte) || isdigit(scanner->byte) ||
		scanner->byte == '_') {
		if (swift_push_char(&string, scanner->byte) < 0 ) {
			shine_error_free(string, ERROR_NO_MEMORY);
		}

		shine_next_scanner_byte(scanner);
	}

	if (string == NULL)
		return ERROR_NULL_STRING;

	if (isspace(scanner->byte) || scanner->byte != ':') {
		shine_error_free(string, ERROR_INVALID_ID_ENDING);
	} else
		shine_next_scanner_byte(scanner);

	if (!(*str = swift_strndup(string->data, string->len))) {
		shine_error_free(string, ERROR_NO_MEMORY);
	}

	shine_error_free(string, ERROR_NONE);
}

static int shine_parse_token(struct shine_scanner *scanner)
{
	switch (scanner->byte) {
		case '\0': return shine_consume_byte(scanner, TOKEN_END);
		case ',': return shine_consume_byte(scanner, TOKEN_COMMA);
		case ';': return shine_consume_byte(scanner, TOKEN_SEMICOLON);
		case '(': return shine_consume_byte(scanner, TOKEN_LPAREN);
		case ')': return shine_consume_byte(scanner, TOKEN_RPAREN);
		case '[': return shine_consume_byte(scanner, TOKEN_LSQUARE);
		case ']': return shine_consume_byte(scanner, TOKEN_RSQUARE);
		case '{': return shine_consume_byte(scanner, TOKEN_LBRACE);
		case '}': return shine_consume_byte(scanner, TOKEN_RBRACE);
		case '"': return shine_consume_byte(scanner, TOKEN_QUOTATION);
		case '/': return shine_parse_comment(scanner);
	}

	if (isspace(scanner->byte))
		return shine_parse_whitespace(scanner);

	if (isdigit(scanner->byte))
		return TOKEN_CONST;

	if (isalpha(scanner->byte))
		return TOKEN_STR;

	return 0;
}

static struct item_data *create_item_data(struct script_obj *newf)
{
	struct item_data *data = calloc(1, sizeof(struct item_data));

	if (!data)
		return NULL;

	if (!(data->name = calloc(strlen(newf->name) + 1, sizeof(char))))
		goto err_free_data;

	if (!(data->url = calloc(strlen(newf->url) + 1, sizeof(char))))
		goto err_free_name;

	if (!(data->path = calloc(strlen(newf->path) + 1, sizeof(char))))
		goto err_free_url;

	swift_init_list(&data->node);

	memcpy(data->name, newf->name, strlen(newf->name) + 1);
	memcpy(data->url, newf->url, strlen(newf->url) + 1);

	if (newf->path)
		memcpy(data->path, newf->path, strlen(newf->path) + 1);

	return data;

err_free_url:
	free(data->url);
err_free_name:
	free(data->name);
err_free_data:
	free(data);

	return NULL;
}

static void unload_script_obj(struct script_obj *obj)
{
	if (!obj)
		return;

	if (obj->url)
		free(obj->url);

	if (obj->name)
		free(obj->name);

	if (obj->path)
		free(obj->path);

	fclose(obj->scanner.file);
}

static void clear_script_obj(struct script_obj *obj)
{
	if (!obj)
		return;

	if (obj->url) {
		shine_free(obj->url);
	}

	if (obj->name) {
		shine_free(obj->name);
	}

	if (obj->path) {
		shine_free(obj->path);
	}
}

static int shine_parse_obj_props(struct script_obj *obj)
{
	char *id = NULL, *str = NULL;
	int token, data, ret;
	struct shine_property *prop;

	token = shine_parse_token(&obj->scanner);

	while (token == TOKEN_STR) {
		if ((ret = shine_parse_identifier(&obj->scanner, &id)) < 0)
			return ret;

		while (id) {
			if ((prop = shine_find_prop(id))) {
				switch (prop->type) {
					case TYPE_INT: {
						data = 0;

						if ((ret = shine_parse_const(&obj->scanner, &data))
							< 0) {
							shine_error_free(id, ret);
						}

						if (shine_set_obj_prop(obj, id, data) == -1) {
							shine_error_free(id, ERROR_INVALID_ID);
						}
					} break;

					case TYPE_STR: {
						if ((ret = shine_parse_string(&obj->scanner, &str))
							< 0) {
							shine_error_free(id, -1);
						}

						if (shine_set_obj_prop(obj, id, str) == -1) {
							shine_free(id);
							shine_error_free(str, ERROR_INVALID_ID);
						}
					} break;

					default: 
						shine_error_free(id, ERROR_UNKNOWN);
				}

				shine_free(id);
			} else {
				if (shine_find_id(id) != TYPE_NONE) {
					if (!(obj->next_id = swift_strndup(id, strlen(id)))) {
						shine_error_free(id, ERROR_NO_MEMORY);
					}

					shine_free(id);
					return ERROR_NONE;
				} else
					return ERROR_INVALID_ID;
			}
		}

		token = shine_parse_token(&obj->scanner);
	}

	if (token == ERROR_COMMENT_BROKEN)
		return ERROR_COMMENT_BROKEN;

	if (token != TOKEN_END)
		return ERROR_INVALID_ID;

	return ERROR_NONE;
}

static int shine_parse_next_obj(struct script_obj *obj)
{
	char *str = NULL;
	int token = TOKEN_STR, ret;

	clear_script_obj(obj);

	if (!obj->next_id)
		token = shine_parse_token(&obj->scanner);

	switch (token) {
		case TOKEN_STR: {
			if (obj->next_id) {
				if (!(str = swift_strndup(obj->next_id, strlen(obj->next_id))))
				{
					shine_free(obj->next_id);
					shine_error_msg(obj->scanner.cursor, ERROR_NO_MEMORY);
				}

				shine_free(obj->next_id);
			}
			else {
				if ((ret = shine_parse_identifier(&obj->scanner, &str)) < 0) {
					shine_error_msg(obj->scanner.cursor, ret);
				}
			}

			if (!str) {
				shine_error_msg(obj->scanner.cursor, ERROR_NULL_STRING);
			}

			obj->type = shine_find_id(str);
			shine_free(str);

			if ((ret = shine_parse_string(&obj->scanner, &str)) < 0) {
				shine_error_msg(obj->scanner.cursor, ret);
			}

			switch (obj->type) {
				case TYPE_PATH: {
					obj->path = swift_strndup(str, strlen(str));
					shine_free(str);
				} break;

				case TYPE_FILE: {
					obj->name = swift_strndup(str, strlen(str));
					shine_free(str);

					if ((ret = shine_parse_obj_props(obj)) < 0) {
						shine_error_msg(obj->scanner.cursor, ret);
					}
				} break;

				default: {
					shine_free(str);
					shine_error_msg(obj->scanner.cursor, ERROR_UNKNOWN);
				}
			}
		} break;

		case ERROR_COMMENT_BROKEN: {
			shine_error_msg(obj->scanner.cursor, ERROR_COMMENT_BROKEN);
		}
		
		case TOKEN_END: {
			obj->type = TYPE_NONE;
		} break;
		
		default: shine_error_msg(obj->scanner.cursor, ERROR_UNKNOWN);
	}

	return ERROR_NONE;
}

static int init_script_obj(struct script_obj *obj, char *path)
{
	FILE *fp = fopen(path, "r");

	if (!fp)
		return -2;

	if (shine_init_scanner(&obj->scanner, fp) < 0)
		return -1;

	obj->name = obj->path = obj->url = obj->next_id = NULL;
	obj->ver = obj->type = 0;

	return 0;
}

void unload_item(struct item_data *data)
{
	if (!data)
		return;

	if (data->name)
		free(data->name);

	if (data->url)
		free(data->url);

	if (data->path)
		free(data->path);

	swift_remove_list_item(&data->node);
	free(data);
}

int parse_updates(struct swift_list *list)
{
	struct script_obj oldf, newf;
	struct item_data *data;
	struct swift_str *str = NULL;
	int count = 0, ret;

	if (!list)
		return -1;

	if ((ret = init_script_obj(&oldf, OLD_SCRIPT)) == -1)
		return -1;

	if (init_script_obj(&newf, NEW_SCRIPT) < 0)
		goto err_free_obj1;

	if (ret == -2) {
		while (shine_parse_next_obj(&newf) == 0) {
			switch (newf.type) { 
				case TYPE_PATH: { 
					create_folder(newf.path); 
				} break; 

				case TYPE_FILE: { 
					if (!(data = create_item_data(&newf))) { 
						goto err_free_obj1; 
					}

					swift_push_list_item(list, &data->node); 
					count++; 
				} break; 
			} 

			if (newf.type == TYPE_NONE) 
				break;
		}
	}
	else {
		while (shine_parse_next_obj(&newf) == 0) {
			if (shine_parse_next_obj(&oldf) == 0) {
				if (newf.name)
					swift_push_strf(&str, "%s%s", newf.path, newf.name);
				else
					swift_push_strf(&str, "%s", newf.path);

				if (oldf.ver != newf.ver || !file_exists(str->data)) {
					switch (newf.type) { 
						case TYPE_PATH: { 
							create_folder(newf.path); 
						} break; 

						case TYPE_FILE: { 
							if (!(data = create_item_data(&newf))) 
								goto err_free_obj2; 

							swift_push_list_item(list, &data->node); 
							count++; 
						} break; 
					} 

					if (newf.type == TYPE_NONE) 
						break;
				}
			}
			else {
				switch (newf.type) { 
					case TYPE_PATH: { 
						create_folder(newf.path); 
					} break; 

					case TYPE_FILE: { 
						if (!(data = create_item_data(&newf))) 
							goto err_free_obj1; 

						swift_push_list_item(list, &data->node); 
						count++; 
					} break; 
				} 

				if (newf.type == TYPE_NONE) 
					break;
			}

			if (str)
				swift_truncate_str(str, 0);
		}

		unload_script_obj(&oldf);
	}

	if(str)
		swift_free_str(str);

	unload_script_obj(&newf);

	return count;

err_free_obj2:
	if(str)
		swift_free_str(str);

	unload_script_obj(&oldf);
err_free_obj1: {
	struct swift_list *next = NULL, *item = NULL;
	unload_script_obj(&newf);

	swift_foreach_list_item_safe(list, item, next) {
		data = swift_container_of(item, struct item_data, node);

		if (data)
			unload_item(data);
	}
}
	return -1;
}