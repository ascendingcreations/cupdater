/* 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Author(s):
 *  - S.J.R. van Schaik <stephan@synkhronix.com>
 */
#include <scanner.h>
#include <math.h>

static inline void shine_update_cursor(struct shine_scanner *scanner)
{
	switch (scanner->byte) {
		case '\n': {
			++scanner->next_cursor.line;
			scanner->next_cursor.col = 1;
		} break;

		case '\t': {
			scanner->next_cursor.col += scanner->tab_width;
		} break;

		default:
			++scanner->next_cursor.col;
	}
}

int shine_init_scanner(struct shine_scanner *scanner, FILE *file)
{
	if (!scanner || !file)
		return -1;

	scanner->cursor = SHINE_CURSOR(1, 1);
	scanner->next_cursor = SHINE_CURSOR(1, 1);
	scanner->file = file;
	scanner->tab_width = 4;
	scanner->byte = '\0';

	shine_next_scanner_byte(scanner);

	return 0;
}

char shine_next_scanner_byte(struct shine_scanner *scanner)
{
	int ret;

	scanner->cursor = scanner->next_cursor;
	shine_update_cursor(scanner);
	scanner->byte = (ret = fgetc(scanner->file)) == EOF ? '\0' : ret;

	return scanner->byte;
}
