/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Author(s):
 *  - S.J.R. van Schaik <stephan@synkhronix.com>
 *  - Andrew Wheeler <lordsatin@hotmail.com>
 */
#include <stdio.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <windows.h>
#include <direct.h>
#include <file_io.h>
#include <str.h>

void create_folder(const char *path)
{
	if (strcmp(path, "") == 0) {
		printf("Path was empty.\n");
		return;
	}

	if (!(mkdir(path)))
		printf("Path:(%s), was created\n", path);
}

void delete_file(const char *path)
{
	remove(path);
}

void rename_file(const char *path, const char *filename, const char *newname)
{
	struct swift_str *file1 = NULL, *file2 = NULL;

	swift_push_strf(&file1, "%s%s",path, filename);
	swift_push_strf(&file2, "%s%s",path, newname);
	rename(file1->data, file2->data);
	swift_free_str(file1);
	swift_free_str(file2);
}

int download_file(const char *url, const char *output)
{
	CURL *curl;
	FILE *fp;
	CURLcode res = 0;

	if ((curl = curl_easy_init())) {
		fp = fopen(output, "wb");
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
		res = curl_easy_perform(curl);
		curl_easy_cleanup(curl);
		fclose(fp);
	}

	return res;
}

int run_process(const char *path)
{
	PROCESS_INFORMATION pif;
	STARTUPINFO si = {0};
	BOOL ret;

	si.cb = sizeof(si);

	if (!(ret = CreateProcess(path, NULL, NULL, NULL, FALSE, 0, NULL, NULL, &si,
		&pif))) {
		MessageBox(HWND_DESKTOP, "Unable to start program", "", MB_OK);
		return 1;
	}

	CloseHandle(pif.hProcess);
	CloseHandle(pif.hThread);

	return 0;
}

char *load_file(const char *path)
{
	FILE *file = fopen(path, "r");
	size_t filesize = 0;
	char *data = NULL;

	fseek(file, 0, SEEK_END);
	filesize = ftell(file);
	rewind(file);

	data = calloc(1, sizeof(char) * (filesize + 1));
	fread(data, sizeof(char), filesize, file);
	data[filesize] = '\0';
	fclose(file);

	return data;
}

bool file_exists(const char *path)
{
	FILE *fp;
	fpos_t fsize = 0;

	if (!fopen_s(&fp, path, "r")) {
		fseek(fp, 0, SEEK_END);
		fgetpos(fp, &fsize);
		fclose(fp);
	}

	return fsize > 0;
}