#include <obj_props.h>
#include <phraser.h>
#include <stddef.h>
#include <string.h>
#include <stdarg.h>

struct shine_property properties[] = {
	{ "path", TYPE_STR, offsetof(struct script_obj, path) },
	{ "url", TYPE_STR, offsetof(struct script_obj, url) },
	{ "version", TYPE_INT, offsetof(struct script_obj, ver) },
	{ NULL, 0, 0 },
};

struct shine_id_types id_types[] = {
	{ "dir", TYPE_PATH },
	{ "file", TYPE_FILE },
	{ NULL, 0},
};

int shine_find_id(const char *name)
{
	struct shine_id_types *type;

	if (!name)
		return ERROR_INVALID_ID;

	for (type = id_types; type->name; ++type) {
		if (strcmp(type->name, name) == 0)
			return type->type;
	}

	return TYPE_NONE;
}

struct shine_property *shine_find_prop(const char *name)
{
	struct shine_property *property;

	if (!name)
		return NULL;

	for (property = properties; property->name; ++property) {
		if (strcmp(property->name, name) == 0)
			return property;
	}

	return NULL;
}

int shine_set_obj_prop(struct script_obj *obj, const char *name, ...)
{
	va_list args;
	struct shine_property *property;
	void *p;

	if (!(property = shine_find_prop(name)))
		return -1;

	p = (void *)((uintptr_t)obj + property->offset);

	va_start(args, name);

	switch (property->type) {
		case TYPE_INT: *(int *)p = va_arg(args, int); break;
		case TYPE_STR: *(char **)p = va_arg(args, char *); break;
		default: return -1;
	}

	va_end(args);

	return 0;
}

