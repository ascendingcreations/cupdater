/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Author(s):
 *  - S.J.R. van Schaik <stephan@synkhronix.com>
 */

#include <stdlib.h>

#include <list.h>

void swift_init_list(struct swift_list *list)
{
	list->prev = list->next = list;
}

bool swift_is_list_empty(struct swift_list *list)
{
	return !list || list->prev == list || list->next == list;
}

int swift_insert_list_item_before(struct swift_list *node,
	struct swift_list *item)
{
	if (!node || !item)
		return -1;

	item->prev = node->prev;
	node->prev = item;

	item->prev->next = item;
	item->next = node;

	return 0;
}

int swift_insert_list_item_after(struct swift_list *node,
	struct swift_list *item)
{
	if (!node || !item)
		return -1;

	item->next = node->next;
	node->next = item;
	
	item->next->prev = item;
	item->prev = node;

	return 0;
}

int swift_push_list_item(struct swift_list *list, struct swift_list *item)
{
	return swift_insert_list_item_before(list, item);
}

int swift_unshift_list_item(struct swift_list *list, struct swift_list *item)
{
	return swift_insert_list_item_after(list, item);
}

int swift_remove_list_item(struct swift_list *item)
{
	if (!item)
		return -1;

	item->prev->next = item->next;
	item->next->prev = item->prev;
	
	item->next = item;
	item->prev = item;

	return 0;
}

struct swift_list *swift_pop_list_item(struct swift_list *list)
{
	struct swift_list *item;

	if (!list || swift_is_list_empty(list))
		return NULL;

	item = list->prev;
	swift_remove_list_item(item);

	return item;
}

struct swift_list *swift_shift_list_item(struct swift_list *list)
{
	struct swift_list *item;

	if (!list || swift_is_list_empty(list))
		return NULL;

	item = list->next;
	swift_remove_list_item(item);

	return item;
}

