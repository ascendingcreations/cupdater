/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Author(s):
 *  - S.J.R. van Schaik <stephan@synkhronix.com>
 *  - Andrew Wheeler <lordsatin@hotmail.com>
 */
#include <str.h>

#define swift_expand_str(str, len, add) \
		if (!str || len >= (str)->size)  \
			if (!(str = swift_resize_str(str, len + add))) \
				return -1

char *swift_strdup(const char *s)
{
	char *ret;
	size_t n = strlen(s);

	if (!(ret = calloc(n + 1, sizeof(*ret))))
		return NULL;

	return memcpy(ret, s, n);
}

char *swift_strndup(const char *s, size_t n)
{
	char *ret;
	size_t len = strlen(s);

	if (n > len)
		n = len;

	if (!(ret = calloc(n + 1, sizeof(*ret))))
		return NULL;

	return memcpy(ret, s, n);
}

struct swift_str *swift_alloc_str(size_t size)
{
	struct swift_str *str;

	if (!(str = calloc(1, sizeof *str)))
		return NULL;

	str->size = size;

	if (!str->size)
		return str;

	if (!(str->data = malloc(str->size))) {
		free(str);
		return NULL;
	}

	return str;
}

struct swift_str *swift_resize_str(struct swift_str *str, size_t size)
{
	size = size < 8 ? 8 : size;

	if (!str)
		return swift_alloc_str(size);

	if (!(str->data = realloc(str->data, size)))
		return NULL;

	str->size = size;
	return str;
}

void swift_free_str(struct swift_str *str)
{
	if (!str)
		return;

	if (str->data)
		free(str->data);

	free(str);
}

bool swift_cmp_str(struct swift_str *lhs, struct swift_str *rhs)
{
	if (!lhs || !rhs || lhs->len > rhs->len || lhs->len < rhs->len)
		return false;

	return memcmp(lhs->data, rhs->data, lhs->len) == 0 ? true : false;
}

struct swift_str *swift_clone_str(struct swift_str *str)
{
	struct swift_str *clone = swift_alloc_str(str->len);

	memcpy(clone->data, str->data, str->len);
	clone->len = str->len + 1;

	return clone;
}

int swift_push_char(struct swift_str **lhs, const char rhs)
{
	size_t len;

	if (!lhs)
		return -1;

	if (!rhs)
		return 0;

	len = *lhs ? (*lhs)->len + 1: 1;

	swift_expand_str(*lhs, len, 0);

	(*lhs)->data[(*lhs)->len++] = rhs;
	(*lhs)->len = len;

	return 0;
}

int swift_push_cstr(struct swift_str **lhs, const char *rhs)
{
	size_t len;

	if (!lhs)
		return -1;

	if (!rhs)
		return 0;

	len = *lhs ? strlen(rhs) + (*lhs)->len : strlen(rhs);

	swift_expand_str(*lhs, len, 1);

	memcpy((*lhs)->data + (*lhs)->len, rhs, strlen(rhs) + 1);
	(*lhs)->len = len;
	return 0;
}

int swift_push_str(struct swift_str **lhs, struct swift_str *rhs)
{
	size_t len;

	if (!lhs)
		return -1;

	if (!rhs)
		return 0;

	len = *lhs ? rhs->len + (*lhs)->len : rhs->len;

	swift_expand_str(*lhs, len, 1);

	memcpy((*lhs)->data + (*lhs)->len, rhs->data, rhs->len + 1);
	(*lhs)->len = len;

	return 0;
}

int swift_push_strf(struct swift_str **str, const char *fmt, ...)
{
	va_list args;
	size_t len;
	char *data;

	if (!str)
		return -1;

	va_start(args, fmt);
	len = vsnprintf(NULL, 0, fmt, args);
	va_end(args);

	data = malloc(len);

	va_start(args, fmt);
	vsnprintf(data, len + 1, fmt, args);
	va_end(args);

	len = *str ? (*str)->len + len : len;

	swift_expand_str(*str, len, 1);

	memcpy((*str)->data + (*str)->len, data, strlen(data) + 1);
	(*str)->len = len;
	free(data);
	return 0;
}

int swift_join_cstrs(struct swift_str **str, size_t count, char **strs,
	char *sep)
{
	size_t i, size = 0;

	if (!str || !*str || !strs || !sep || !count)
		return -1;

	for (i = 0; i < count; i++) {
		if (!strs[i])
			continue;

		size += strlen(strs[i]);
	}

	size += (strlen(sep) * count);

	if (!(*str = swift_resize_str(*str, (*str)->size + size)))
		return -1;

	for (i = 0; i < count - 1; ++i) {
		swift_push_cstr(str, strs[i]);
		swift_push_cstr(str, sep);
	}

	swift_push_cstr(str, strs[i]);

	return 0;
}

int swift_substr(struct swift_str *str, size_t offset, size_t len)
{
	if (!str)
		return 0;

	offset = offset < str->len ? offset : str->len;
	len = len < str->len - offset ? len : str->len - offset;

	memmove(str->data, str->data + offset, len);
	str->len = len;

	return 0;
}

int swift_str_subst(struct swift_str **tgt, const char *pat, const char *repl)
{
	struct swift_str *new_str, *old_str;
	size_t i = 0, pat_len = strlen(pat);
	int ret;

	if (!tgt && !*tgt)
		return -1;

	old_str = *tgt;

	if (!(new_str = swift_alloc_str(old_str->len)))
		return -1;

	while (i + pat_len <= old_str->len) {
		if (memcmp(old_str->data + i, pat, pat_len) != 0) {
			if ((ret = swift_push_char(&new_str, old_str->data[i])) < 0)
				goto err_free_new_str;

			i++;
		}
		else {
			if ((ret = swift_push_cstr(&new_str, repl)) < 0)
				goto err_free_new_str;

			i += pat_len;

			if (!pat_len) {
				if ((ret = swift_push_char(&new_str, old_str->data[i++])) < 0)
					goto err_free_new_str;
			}
		}
	}

	while (i < old_str->len) {
		if ((ret = swift_push_char(&new_str, old_str->data[i++])) < 0)
			goto err_free_new_str;
	}

	swift_free_str(old_str);
	*tgt = new_str;
	return 0;

err_free_new_str:
	swift_free_str(new_str);
	return ret;
}

int swift_truncate_str(struct swift_str *str, size_t len)
{
	if (!str)
		return 0;

	len = len < str->len ? len : str->len;
	str->len = len;
	str->data[len] = '\0';
	return 0;
}
