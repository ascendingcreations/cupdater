#include <stdio.h>
#include <file_io.h>
#include <phraser.h>
#include <str.h>
#include <macros.h>

static void unload_list(struct swift_list *list)
{
	struct swift_list *item, *next;
	struct item_data *data;

	if (!list)
		return;

	swift_foreach_list_item_safe(list, item, next) {
		data = swift_container_of(item, struct item_data, node);

		unload_item(data);
	}
}

int updater(void)
{
	struct swift_list root, *item, *next;
	struct item_data *data;
	struct swift_str *str = NULL;
	int i = 0, count = 0, ret = 0;

	swift_init_list(&root);

	if ((count = parse_updates(&root)) == -1) {
		puts("Unable to check updates");
		return -1;
	}

	if (count > 0) {
		printf("%i files to download. \n", count);

		swift_foreach_list_item_safe(&root, item, next) {
			data = swift_container_of(item, struct item_data, node);
			create_folder(data->path);
			swift_push_strf(&str, "%s%s", data->path, data->name);

			if ((ret = download_file(data->url, str->data)) > 0) {
				printf("Unable to download %s, Error Code %i\n", data->name,
					ret);
				goto err_unload_data;
			}

			swift_truncate_str(str, 0);
			unload_item(data);
			i++;
			printf("%i out of %i files downloaded. \n", i, count);
		}
	}

	if (str)
		swift_free_str(str);

	return 0;

err_unload_data:
	unload_list(&root);

	if (str)
		swift_free_str(str);

	return -1;
}

int main(void)
{
	int ret = 0;

	puts("Checking For Updates");

	if ((ret = download_file(URL, NEW_SCRIPT)) > 0) {
		printf("Failed to retrieve update file. error code is %i\n", ret);
		goto err_occured;
	}

	if (updater() < 0) {
		puts("An Error Occured");
		goto err_occured;
	}

	puts("Updates completed");

	if (file_exists(OLD_SCRIPT))
		delete_file(OLD_SCRIPT);

	rename_file("", NEW_SCRIPT, OLD_SCRIPT);
	puts("Pressed Enter to close");
	getchar();
	return 0 ;//run_process(path);

err_occured:
	puts("Pressed Enter to close");
	getchar();
	return -1;
}

